# ighv-capture

With PacBio capture data on the human immunoglobulin heavy chain locus, you can use `ighv-capture` to:

1. Create a coverage profile over your samples (this is just a fancy way to say create a bedgraph)
2. Call SNPs
3. Phase SNPs
4. Phase the PacBio reads
5. Assembly the PacBio reads
6. Call structural variants
7. Genotype IGHV genes
8. Generate population statistics

The only input to `ighv-capture` is a configuration file. The configuration file has all the parameters for `ighv-capture` and a full path to all the `*bax.h5` files. 

The advantages of using `ighv-capture` are:

1. It will check if a sample has already been processed. If it has then it will do nothing to it. Therefore, you can have only one configuration file for your whole project, and add samples to it as you sequence more individuals.
2. It will update the population statistics everytime you run it.
3. You can run it in the cluster or locally.

Running `ighv-capture`:
```
ighv-capture config.json
```