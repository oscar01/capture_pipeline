#!/bin/env python
import os
import sys
import json
import subprocess

READS_TO_FRANKEN_PREFIX = "reads_to_franken"
IGHV_PREFIX = "IGHV"
MAPPING_READS_DIR = "mapping_reads"

def non_emptyfile(checkfile):
    return os.path.isfile(checkfile) and os.path.getsize(checkfile) > 0

class Sample:
    def __init__(self,sample_name,sample_baxs,scratch,project,franken):
        ### Input variables
        self.sample_name = sample_name
        self.scratch = scratch
        self.project = project
        self.franken = franken
        self.bax_files = sample_baxs
        
        ### Created variables
        self.map_reads_commands = []
        self.coverage_profile_commands = []
        self.call_snps_commands = []
        self.phased_vcffile_commands = []
        
        ## Files
        self.mapped_reads = "%s/%s/%s.sorted.bam" % (self.project,self.sample_name,READS_TO_FRANKEN_PREFIX)
        self.coverage_profile = "%s/%s/%s.bedgraph" % (self.project,self.sample_name,READS_TO_FRANKEN_PREFIX)
        self.vcffile = "%s/%s/%s.vcf" % (self.project,self.sample_name,READS_TO_FRANKEN_PREFIX)
        self.phased_vcffile = "%s/%s/%s.phased.vcf" % (self.project,self.sample_name,READS_TO_FRANKEN_PREFIX)
        self.phased_mapped_reads = "%s/%s/%s.phased.sorted.bam" % (self.project,self.sample_name,READS_TO_FRANKEN_PRFIX)
        self.assembled_locus_fasta = "%s/%s/%s.assembled.fasta" % (self.project,self.sample_name,IGHV_PREFIX)
        self.indels_and_svs = "%s/%s/%s.indels_and_svs.bed" % (self.project,self.sample_name,IGHV_PREFIX)
        self.ighv_alleles = "%s/%s/%s.alleles.tab" % (self.project,self.sample_name,IGHV_PREFIX)
        
        ### Functions
        self.jobs = self.jobs_to_be_done()
        
    def jobs_to_be_done(self):
        jobs = []
        if not non_emptyfile(self.mapped_reads):
            self.map_reads()
            jobs.append(self.map_reads_commands)
        if not non_emptyfile(self.coverage_profile):
            self.create_bedgraph()
            jobs.append(self.create_bedgraph_commands)
        if not non_emptyfile(self.vcffile):
            self.call_snps()
            jobs.append(self.call_snps_commands)
        if not non_emptyfile(self.phased_vcffile):
            self.phase_snps()
            jobs.append(self.phased_vcffile_commands)
        return jobs
    
    def create_bax_fofn(self):
        bax_fofns = []
        for run_index in self.bax_files:
            bax_fofn = "%s/%s/%s/%s/bax.fofn" % (self.scratch,self.sample,MAPPING_READS_DIR,run_index)
            with open(bax_fofn,'w') as bax_fofh:
                for i in range(1,4):
                    bax_fofh.write("%s" % self.bax_files[run_index][i])
            bax_fofns.append(bax_fofn)
        return bax_fofns
    
    def turn_bax_to_bam(self,bax_fofns):
        bam_files = []
        for bax_fofn in bax_fofns:
            output_dir = "%s/%s/%s/%s" % (self.scratch,self.sample,MAPPING_READS_DIR,run_index)
            command = "bax2bam --fofn=%s -o %s/bax2bam" % (bax_fofn,output_dir)
            self.map_reads_commands.append(command)
            bam_file = "%s/bax2bam.subreads.bam" % output_dir
            bam_files.append(bam_file)
        return bam_files
    
    def create_xml_file(self,bam_files):
        output_dir = "%s/%s/%s/%s" % (self.scratch,self.sample,MAPPING_READS_DIR,run_index)
        xml_file = "%s/subreads.xml" % output_dir
        command = "dataset create --type SubreadSet %s %s" % (xml_file," ".join(bam_files))
        self.map_reads_commands.append(command)
        return xml_file
        
    def map_reads(self):
        bax_fofns = self.create_bax_fofn()
        bam_files = self.turn_bax_to_bam(bax_fofns)
        xml_file = self.create_xml_file(bam_files)
        command = """
        blasr
        """ % (xml_file,)
        self.map_reads_commands.append(command)

    def create_bedgraph(self):
        command = """
        bedtools
        """ % (
        self.create_bedgraph_commands.append(command)
        
    def call_snps(self):
        command = """
        quiver 
        """
        self.call_snps_commands.append(command)
        
    def phase_snps(self):
        command = """
        whatshap
        """
        self.phased_vcffile_commands.append(command)

def run_pipeline(json_file):
    samples = []
    data = json.loads(json_file)
    franken = create_reference(data)    
    scratch = data["scratch"]
    project = data["project"]
    for sample_name in data["bax"]:
        sample = Sample(data["bax"][sample_name]["run_index"],scratch,project,franken)
        samples.append(sample)
    jobs = get_jobs(samples)
    for set_of_jobs in jobs:
        submit_jobs(set_of_jobs)

def main():
    run_pipeline(sys.argv[1])


    


def create_file_structure():
    pass



def create_bax_fofn(output_dir,bax_files):
    bax_fofn = "%s/bax.fofn" % output_dir
    with open(bax_fofn,'w') bax_fofh:
        for bax_file in bax_files:
            bax_fofh.write("%s\n" % bax_file)
    return bax_fofn

def turn_bax_to_bam(output_dir,bax_fofn):
    command = "bax2bam --fofn=%s -o %s/bax2bam" % (bax_fofn,output_dir)
    subprocess.call(command,shell=True)
    bam_file = "%s/bax2bam.subreads.bam"
    return bam_file
    
def get_bam_files(runs,sample_dir):
    bam_files = []
    for run_index in runs:
        output_dir = "%s/%s" % (sample_dir,run_index)
        bax_files = runs[run_index]
        bax_fofn = create_bax_fofn(output_dir,bax_files)
        bam_file = turn_bax_to_bam(output_dir,bax_fofn)
        bam_files.append(bam_file)
    return bam_files

def create_xml_files(data):
    for sample in data["bax"]:
        sample_dir = "%s/%s" % (data["scratch"],sample)
        xml_file = "%s/subreads.xml" % sample_dir
        if non_empty_file(xml_file):
            continue
        bam_files = get_bam_files(data["bax"][sample]["run_index"],sample_dir)
        command = "dataset create --type SubreadSet %s %s" % (xml_file," ".join(bam_files))
        subprocess.call(command,shell=True)

def map_reads():
    pass

def call_snps():
    pass

def phase_snps():
    pass

def phase_reads():
    pass

def create_bedgraphs():
    pass

def assemble_reads():
    pass

def run_pipeline(json_file):
    data = json.loads(json_file)
    create_xml_files(data)

def main():
    run_pipeline(sys.argv[1])

    
